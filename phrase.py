import random


class Phrase:

    def __init__(self):
        phrase_list = [
            "AFGANISTAN",
            "ALBANIA",
            "ALGIERIA",
            "ANDORA",
            "ANGOLA",
            "ANTIGUA I BARBUDA",
            "ARABIA SAUDYJSKA",
            "ARGENTYNA",
            "ARMENIA",
            "AUSTRALIA",
            "AUSTRIA",
            "AZERBEJDŻAN",
            "BAHAMY",
            "BAHRAJN",
            "BANGLADESZ",
            "BARBADOS",
            "BELGIA",
            "BELIZE",
            "BENIN",
            "BHUTAN",
            "BIAŁORUŚ",
            "BOLIWIA",
            "BOŚNIA I HERCEGOWINA",
            "BOTSWANA",
            "BRAZYLIA",
            "BRUNEI",
            "BUŁGARIA",
            "BURKINA FASO",
            "BURUNDI",
            "CHILE",
            "CHINY",
            "CHORWACJA",
            "CYPR",
            "CZAD",
            "CZARNOGÓRA",
            "CZECHY",
            "DANIA",
            "DEMOKRATYCZNA REPUBLIKA KONGA",
            "DOMINIKA",
            "DOMINIKANA",
            "DŻIBUTI",
            "EGIPT",
            "EKWADOR",
            "ERYTREA",
            "ESTONIA",
            "ESWATINI",
            "ETIOPIA",
            "FIDŻI",
            "FILIPINY",
            "FINLANDIA",
            "FRANCJA",
            "GABON",
            "GAMBIA",
            "GHANA",
            "GRECJA",
            "GRENADA",
            "GRUZJA",
            "GUJANA",
            "GWATEMALA",
            "GWINEA",
            "GWINEA BISSAU",
            "GWINEA RÓWNIKOWA",
            "HAITI",
            "HISZPANIA",
            "HOLANDIA",
            "HONDURAS",
            "INDIE",
            "INDONEZJA",
            "IRAK",
            "IRAN",
            "IRLANDIA",
            "ISLANDIA",
            "IZRAEL",
            "JAMAJKA",
            "JAPONIA",
            "JEMEN",
            "JORDANIA",
            "KAMBODŻA",
            "KAMERUN",
            "KANADA",
            "KATAR",
            "KAZACHSTAN",
            "KENIA",
            "KIRGISTAN",
            "KIRIBATI",
            "KOLUMBIA",
            "KOMORY",
            "KONGO",
            "KOREA POŁUDNIOWA",
            "KOREA PÓŁNOCNA",
            "KOSTARYKA",
            "KUBA",
            "KUWEJT",
            "LAOS",
            "LESOTHO",
            "LIBAN",
            "LIBERIA",
            "LIBIA",
            "LIECHTENSTEIN",
            "LITWA",
            "LUKSEMBURG",
            "ŁOTWA",
            "MACEDONIA PÓŁNOCNA",
            "MADAGASKAR",
            "MALAWI",
            "MALEDIWY",
            "MALEZJA",
            "MALI",
            "MALTA",
            "MAROKO",
            "MAURETANIA",
            "MAURITIUS",
            "MEKSYK",
            "MIKRONEZJA",
            "MJANMA",
            "MOŁDAWIA",
            "MONAKO",
            "MONGOLIA",
            "MOZAMBIK",
            "NAMIBIA",
            "NAURU",
            "NEPAL",
            "NIEMCY",
            "NIGER",
            "NIGERIA",
            "NIKARAGUA",
            "NORWEGIA",
            "NOWA ZELANDIA",
            "OMAN",
            "PAKISTAN",
            "PALAU",
            "PANAMA",
            "PAPUA-NOWA GWINEA",
            "PARAGWAJ",
            "PERU",
            "POLSKA",
            "POŁUDNIOWA AFRYKA",
            "PORTUGALIA",
            "REPUBLIKA ŚRODKOWOAFRYKAŃSKA",
            "REPUBLIKA ZIELONEGO PRZYLĄDKA",
            "ROSJA",
            "RUMUNIA",
            "RWANDA",
            "SAINT KITTS I NEVIS",
            "SAINT LUCIA",
            "SAINT VINCENT I GRENADYNY",
            "SALWADOR",
            "SAMOA",
            "SAN MARINO",
            "SENEGAL",
            "SERBIA",
            "SESZELE",
            "SIERRA LEONE",
            "SINGAPUR",
            "SŁOWACJA",
            "SŁOWENIA",
            "SOMALIA",
            "SRI LANKA",
            "STANY ZJEDNOCZONE",
            "SUDAN",
            "SUDAN POŁUDNIOWY",
            "SURINAM",
            "SYRIA",
            "SZWAJCARIA",
            "SZWECJA",
            "TADŻYKISTAN",
            "TAJLANDIA",
            "TANZANIA",
            "TIMOR WSCHODNI",
            "TOGO",
            "TONGA",
            "TRYNIDAD I TOBAGO",
            "TUNEZJA",
            "TURCJA",
            "TURKMENISTAN",
            "TUVALU",
            "UGANDA",
            "UKRAINA",
            "URUGWAJ",
            "UZBEKISTAN",
            "VANUATU",
            "WATYKAN",
            "WENEZUELA",
            "WĘGRY",
            "WIELKA BRYTANIA",
            "WIETNAM",
            "WŁOCHY",
            "WYBRZEŻE KOŚCI SŁONIOWEJ",
            "WYSPY MARSHALLA",
            "WYSPY SALOMONA",
            "WYSPY ŚWIĘTEGO TOMASZA I KSIĄŻĘCA",
            "ZAMBIA",
            "ZIMBABWE",
            "ZJEDNOCZONE EMIRATY ARABSKIE"
        ]
        self.phrase = random.choice(phrase_list)

        self.consonants_to_guess = self.create_consonants_to_guess(self.phrase)
        self.vowels_to_guess = self.create_vowels_to_guess(self.phrase)

        self.phrase_to_guess = self.create_phrase_to_guess(self.phrase)
        self.phrase_guessed_for_now = self.create_phrase_guessed_for_now(self.phrase_to_guess)

    def create_phrase_to_guess(self, phrase):
        phrase_to_guess = []
        for i in phrase:
            phrase_to_guess.append(i)
        return phrase_to_guess

    def create_phrase_guessed_for_now(self, phrase_to_guess):
        phrase_guessed_for_now = phrase_to_guess[:]
        for i in range(len(phrase_to_guess)):
            value = phrase_to_guess[i]
            if value not in (" ", "-"):
                phrase_guessed_for_now[i] = "_"
        return phrase_guessed_for_now

    def reveal_letter(self, phrase_to_guess, phrase_guessed_for_now, letter):
        new_phrase_guessed_for_now = phrase_guessed_for_now[:]
        counter = 0
        for i in range(len(phrase_guessed_for_now)):
            if phrase_to_guess[i] == letter:
                new_phrase_guessed_for_now[i] = letter
                counter += 1
        return [new_phrase_guessed_for_now, counter]

    def print_pretty(self, phrase_guessed_for_now):
        print(" ".join(phrase_guessed_for_now))

    def create_letters_to_guess(self, phrase):
        letters_to_guess = set(phrase)
        letters_to_guess.discard(" ")
        return letters_to_guess

    def create_consonants_to_guess(self, phrase):
        consonants = "BCĆDFGHJKLŁMNŃPRSŚTWZŹŻ"
        letters_to_guess = self.create_letters_to_guess(phrase)
        consonants_to_guess = letters_to_guess.copy()
        for letter in letters_to_guess:
            if letter not in consonants:
                consonants_to_guess.remove(letter)
        return consonants_to_guess

    def create_vowels_to_guess(self, phrase):
        vowels = "AĄEĘIOÓUY"
        letters_to_guess = self.create_letters_to_guess(phrase)
        vowels_to_guess = letters_to_guess.copy()
        for letter in letters_to_guess:
            if letter not in vowels:
                vowels_to_guess.remove(letter)
        return vowels_to_guess
