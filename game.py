from phrase import Phrase
from wheel import Wheel

import os


class Game:

    def __init__(self):
        self.phrase = Phrase()
        self.phrase_to_guess = self.phrase.phrase_to_guess
        self.phrase_guessed_for_now = self.phrase.phrase_guessed_for_now

        self.player_points = 0

        self.wheel = Wheel()

        self.run()

    def run(self):
        print()
        print()
        print("Witaj w grze")
        print("KOŁO FORTUNY")
        print()
        print("Wprowadź swoje imię:")
        self.player_name = input()

        self.action = "Zaczynamy!"
        while True:
            self.print_header()
            print("Co zrobisz? (1 - kręć kołem, 2 - kup samogłoskę, 3 - zgadnij hasło):")
            do = input()

            if do == "1":
                self.action = "Kręcisz kołem. "
                result = self.wheel.turn_the_will()
                if result in (100, 200, 300, 400, 500, 750, 1000, 1500):
                    self.action += "Wylosowałeś " + str(result) + "!"
                    self.print_header()
                    print("Zgadnij spółgłoskę:")
                    letter = input().upper()

                    if letter in self.phrase.consonants_to_guess:
                        self.phrase.consonants_to_guess.remove(letter)
                        [self.phrase_guessed_for_now, counter] = self.phrase.reveal_letter(self.phrase_to_guess,
                                                                                           self.phrase_guessed_for_now,
                                                                                           letter)
                        self.player_points += result * counter

                        self.action = "Zgadłeś!"
                        if len(self.phrase.consonants_to_guess) == 0:
                            self.action += " Koniec samogłosek!"
                        self.print_header()
                    else:
                        self.action = "Nie zgadłeś!"
                elif result == "BANKRUT":
                    self.action += "Wylosowałeś " + str(result) + "!"
                    self.player_points = 0
            elif do == "2":
                self.action = "Chcesz kupić samogłoskę. "
                price = 50
                if self.player_points >= price:
                    self.print_header()
                    print("Wybierz samogłoskę:")
                    letter = input().upper()
                    if letter in self.phrase.vowels_to_guess:
                        self.phrase.vowels_to_guess.remove(letter)
                        [self.phrase_guessed_for_now, counter] = self.phrase.reveal_letter(self.phrase_to_guess,
                                                                                           self.phrase_guessed_for_now,
                                                                                           letter)
                        self.player_points -= 50

                        self.action = "Zgadłeś!"
                        self.print_header()
                    else:
                        self.action = "Nie zgadłeś!"
                elif self.player_points < price:
                    self.action += "Nie masz wystarczającej ilości pieniędzy!"
            elif do == "3":
                self.action = "Zgadujesz hasło"
                self.print_header()
                print("Zgadnij hasło:")
                phrase_guessing = input().upper()
                if phrase_guessing == self.phrase.phrase:
                    self.action = "Zgadłeś hasło!!! Runda zakończona!"
                    self.phrase_guessed_for_now = self.phrase_to_guess
                    self.print_header()
                    print("Zakończ grę wciskając Enter")
                    input()
                    break
                else:
                    self.action = "Nie zgadłeś!"
                    self.print_header()
            else:
                self.action = "Wybierz jeszcze raz"
                self.print_header()

        self.clear()
        print()
        print()
        print("Koniec gry")
        print("KOŁO FORTUNY")
        print()
        print("Wynik:" + " " + self.player_name + " " + str(self.player_points))
        print()
        input()

    def clear(self):
        os.system("cls")

    def print_header(self):
        self.clear()
        print(self.action)
        print()
        print("Kategoria PAŃSTWA ŚWIATA")
        self.phrase.print_pretty(self.phrase_guessed_for_now)
        print()
        print("Gracz:" + " " + self.player_name + " " + str(self.player_points))


game = Game()
